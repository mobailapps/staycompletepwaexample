# Stay Frontpage API Example App

## What
---------

Stay provides an API that let you create your own frontpage and redirect your users to the needed sections of Stay's PWA.

This is an example app that consumes this API, builds a simple frontpage and redirects to the PWA sections. It also handles all Stay notifications (direct alerts, updates in guest's requests, etc.).

## How
---------

### API ###

You can see the API documentation in: 

https://docs.google.com/document/d/1MouVZPWhFygglJH-Ra8kYAqRdW7u_nAwgwh12Ckd9xI/edit?usp=sharing


### Example App ###

The app has three main scenes:

* **FormViewController:** 
It is a simple form to fullfit the fields needed for the front request. The establishment is the only one required.
This viewController gets the front request response and load the 'mainPage' attribute's url. 

* **WebViewViewController:** 
We need to wait to the 'exit-establishment' redirection in order to close the webView.

