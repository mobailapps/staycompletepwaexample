//
//  WebViewViewController.swift
//  StayFrontpageAPIExample
//
//  Created by Alberto Moraga on 21/01/2020.
//  Copyright © 2020 Stay. All rights reserved.
//

import UIKit
import WebKit

class WebViewViewController: UIViewController, WKScriptMessageHandler {    
    public var urlString: String = ""
    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        webView.navigationDelegate = self
        
        if #available(iOS 16.4, *) {
            webView.isInspectable = true
        }
// Uncomment to show JS logs
        let source = "function captureLog(msg) { window.webkit.messageHandlers.logHandler.postMessage(msg); } window.console.log = captureLog;"
        let script = WKUserScript(source: source, injectionTime: .atDocumentEnd, forMainFrameOnly: false)
        webView.configuration.userContentController.addUserScript(script)
        webView.configuration.userContentController.add(self, name: "logHandler")
// -----------------

        loadPWA()
        webView.scrollView.contentInsetAdjustmentBehavior = .never;
    }
    
    public func loadPWA() {
        let url = URL(string: urlString)!
        webView.load(URLRequest(url: url))
    }

    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if message.name == "logHandler" {
            let time = CFAbsoluteTimeGetCurrent() - WebViewViewController.startTime
            print("LOG \(time): \(message.body)")
        }
    }
}

extension WebViewViewController: WKNavigationDelegate {
    static var startTime = 0.0

    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        WebViewViewController.startTime = CFAbsoluteTimeGetCurrent()
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
    }
    
    @available(iOS 13.0, *)
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, preferences: WKWebpagePreferences, decisionHandler: @escaping (WKNavigationActionPolicy, WKWebpagePreferences) -> Void) {
        if let url = navigationAction.request.url, url.absoluteString.contains("exit-establishment") {
            dismiss(animated: true, completion: nil)
        }
        decisionHandler(.allow, preferences)
    }
}
