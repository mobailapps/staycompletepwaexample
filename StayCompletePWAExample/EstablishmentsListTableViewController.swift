//
//  EstablishmentsListTableViewController.swift
//  StayFrontpageAPIExample
//
//  Created by Alberto Moraga on 22/01/2021.
//  Copyright © 2021 Stay. All rights reserved.
//

import UIKit

protocol EstablishmentsListDelegate {
    func establishmentSelected(_ establishment: Dictionary<AnyHashable, Any>)
}

class EstablishmentsListTableViewController: UITableViewController {
    var delegate: EstablishmentsListDelegate?
    public var establishments: Array<Any> = Array()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return establishments.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! EstablishmentCell

        let establishment = establishments[indexPath.row]
        if let establishment = establishment as? [String:Any] {
            let externalId = establishment["externalId"] ?? ""
            let name = establishment["name"] ?? "Error"
//            let internalId = establishment["id"] ?? ""
            cell.titleLabel?.text = "\(name)"
            cell.subtitleLabel?.text = "\(externalId)"
        }

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedEstablishment = establishments[indexPath.row]
        if let selectedEstablishment = selectedEstablishment as? [String:Any] {
            delegate?.establishmentSelected(selectedEstablishment)
        }
    }
}

class EstablishmentCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
}
